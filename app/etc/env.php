<?php
return array (
  'backend' => 
  array (
    'frontName' => 'xxxxxxx',
  ),
  'crypt' => 
  array (
    'key' => 'xxxxxxx',
  ),
  'session' => 
  array (
    'save' => 'db',
  ),
  'db' => 
  array (
    'table_prefix' => 'mgj3_',
    'connection' => 
    array (
      'default' => 
      array (
        'host' => 'xxxxxxx',
        'dbname' => 'xxxxxxx',
        'username' => 'xxxxxxx',
        'password' => 'xxxxxxx',
        'active' => '1',
      ),
    ),
  ),
  'resource' => 
  array (
    'default_setup' => 
    array (
      'connection' => 'default',
    ),
  ),
  'x-frame-options' => 'SAMEORIGIN',
  'MAGE_MODE' => 'production',
  'cache_types' => 
  array (
    'config' => 1,
    'layout' => 1,
    'block_html' => 1,
    'collections' => 1,
    'reflection' => 1,
    'db_ddl' => 1,
    'eav' => 1,
    'customer_notification' => 1,
    'full_page' => 1,
    'config_integration' => 1,
    'config_integration_api' => 1,
    'translate' => 1,
    'config_webservice' => 1,
    'compiled_config' => 1,
  ),
  'install' => 
  array (
    'date' => 'Tue, 20 Sep 2016 23:44:52 +0200',
  ),
);
